#!/bin/sh

# TODO move this in a dedicated test
# assume only one network card
ETH=$(ls /sys/class/net |grep -E 'eth|enp')

if [ -z "$ETH" ];then
    echo "WARN: no network card found"
else
    echo "INFO: configure network card $ETH"
    ln -s /etc/init.d/net.lo /etc/init.d/net.$ETH
    /etc/init.d/net.$ETH start
fi

if [ ! -e /root/kselftest.tar.gz ];then
    case $(uname -m) in
    aarch64)
        KSARCH=arm64
    ;;
    x86_64)
        KSARCH=x86
    ;;
    armv7l)
        KSARCH=arm
        echo "SKIP: kselftests are disabled on ARM"
    ;;
    *)
        echo "SKIP: no kselftests for $(uname -m) arch"
        exit 0
    ;;
    esac
  # try to get kselftest from kselftest repository
  if wget http://storage.kernelci.org/images/selftests/$KSARCH/kselftest.tar.gz ; then
    tar xzf kselftest.tar.gz || exit $?
  else
    tar xzf binary/$KSARCH/kselftest.tar.gz || exit $?
  fi
fi

# show kselftest files for debug
ls -la
cd kselftest || exit $?
ls -la
# run kselftest test
./run_kselftest.sh | tee output.txt
../parser.sh output.txt
